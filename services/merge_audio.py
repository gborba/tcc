import os

from pydub import AudioSegment

from config.settings import SETTINGS


class MergeAudio:
    def __init__(self) -> None:
        pass

    @classmethod
    def merge(cls, files: list) -> str:
        combined = AudioSegment.empty()
        combined_file_name = "merge_"

        for file_path in files:
            combined_file_name = (
                combined_file_name
                + f"{os.path.splitext(os.path.basename(file_path))[0]}_"
            )
            audio = AudioSegment.from_file(file_path)
            combined += audio

        combined.export(
            f"{SETTINGS['audio']['destination_path']}/{combined_file_name}.mp3",
            format="mp3",
        )

        return combined_file_name
