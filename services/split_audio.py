from typing import Literal
from pathlib import Path

import flet as ft

from .demucs import api
from config.settings import SETTINGS


class SplitAudioSources:
    def __init__(self, page: ft.Page) -> None:
        self.page = page
        self.force_stop: bool = False

    def _status_message(
        self,
        file_name: str,
        progress: int,
        status: Literal["IN_PROGRESS", "FINISHED", "ALL_FINISHED"],
    ):
        return {"file_name": file_name, "progress": progress, "status": status}

    def _process_file(self, file_path: str):
        def update_status(result: dict) -> None:
            if self.force_stop:
                self.page.pubsub.send_all_on_topic(
                    "cancel_end",
                    True,
                )
                raise KeyboardInterrupt

            current_progress = 0

            if result.get("segment_offset"):
                current_progress = result["segment_offset"] / result["audio_length"]

            page.pubsub.send_all_on_topic(
                "file_status",
                self._status_message(file_path, current_progress, status="IN_PROGRESS"),
            )
            page.update()

        page = self.page

        separator = api.Separator(model="mdx_extra", callback=update_status)
        origin, separeted = separator.separate_audio_file(file=Path(file_path))

        path_split = file_path.split("\\")
        file_name = path_split[-1]

        for stem, source in separeted.items():
            api.save_audio(
                wav=source,
                path=f"{SETTINGS['audio']['destination_path']}/{stem}_{file_name}",
                samplerate=separator.samplerate,
            )

    def separate(self, files: list[str]) -> None:
        for file in files:
            self._process_file(file)

            self.page.pubsub.send_all_on_topic(
                "file_status",
                self._status_message(file, 100, status="FINISHED"),
            )
            self.page.update()

        self.page.pubsub.send_all_on_topic(
            "file_status",
            self._status_message(None, 100, status="ALL_FINISHED"),
        )
        self.page.update()
