import os
import subprocess

from config.settings import SETTINGS


class MidiManager:
    def __init__(self):
        pass

    @classmethod
    def generate_scores(cls, file: str, file_path: str) -> str:
        try:
            file_name = os.path.splitext(file)[0]
            output_dir = SETTINGS["sheets"]["destination_path"]

            pdf_path = os.path.join(output_dir, f"{file_name}.pdf")

            cls._convert_to_pdf(output_dir, file_name, file_path)

            return pdf_path
        except Exception as e:
            print(f"An error occurred during conversion: {e}")
            return None

    @classmethod
    def _convert_to_pdf(cls, output_dir, file_name, file_path):
        try:
            command = f"musescore4 -o {output_dir}/{file_name}.pdf {file_path}"
            subprocess.run(command)
        except Exception as e:
            print(f"An error occurred while converting to pdf: {e}")
