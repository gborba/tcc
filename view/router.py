from threading import Thread
from time import sleep

import flet as ft

from config.settings import get_files, SETTINGS
from .views import Views


class ViewRouter(ft.Row):
    def __init__(self, app, page: ft.Page, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.app = app
        self.page = page

        self.sheets_files = get_files(
            path=SETTINGS["sheets"]["destination_path"], extensions=[".midi", ".xml"]
        )
        self.audio_files = get_files(
            path=SETTINGS["audio"]["destination_path"], extensions=[".mp3", ".wav"]
        )

        self.views = Views(self.page)

        self.audio_view = self.views.build_audio_view()
        self.sheet_generator_view = self.views.build_sheet_generator_view()
        self.sheets_view = self.views.build_sheets_view(self.sheets_files)
        self.settings_view = self.views.build_settings_view()

        self._active_view: ft.Control = self.audio_view
        self.controls = [self.active_view]

        self.file_watcher_thread = Thread(target=self.file_watcher)
        self.file_watcher_thread.start()

    def file_watcher(self):
        while True:
            if self.active_view == self.sheets_view:
                self._update_view_content("sheets", self.views.build_sheets_view)

            sleep(0.5)

    def _update_view_content(self, module, build_view_function):
        current_files = get_files(
            path=SETTINGS[module]["destination_path"],
            extensions=[".mp3", ".wav"] if module == "audio" else [".pdf", "xml"],
        )

        if getattr(self, f"{module}_files") != current_files:
            setattr(self, f"{module}_files", current_files)
            setattr(self, f"{module}_view", build_view_function(current_files))
            getattr(self, f"set_{module}_view")()

    @property
    def active_view(self):
        return self._active_view

    @active_view.setter
    def active_view(self, view):
        self._active_view = view
        self.controls[-1] = self._active_view
        self.update()

    def set_audio_view(self):
        self.active_view = self.audio_view
        self.page.bottom_appbar.visible = True
        self.page.update()

    def set_sheet_generator_view(self):
        self.active_view = self.sheet_generator_view
        self.page.bottom_appbar.visible = False
        self.page.update()

    def set_sheets_view(self):
        self.active_view = self.sheets_view
        self.page.bottom_appbar.visible = False
        self.page.update()

    def set_settings_view(self):
        self.active_view = self.settings_view
        self.page.bottom_appbar.visible = False
        self.page.update()
