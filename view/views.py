import os

import flet as ft

from services import MidiManager
from model import BottomBar, SelectAudios, AudioController, Rail

from config.settings import (
    SETTINGS,
    SettingsFileManager,
    update_destination_path,
    update_language,
    update_bit_rate,
    update_bit_depth,
    update_input,
    update_sheets_style,
    update_sheets_font_size,
    update_app_font_scale,
    label,
)


class PageSingleton:
    def __init__(self, page: ft.Page):
        self.page = page

    def update(self):
        self.page.update()


class Views:
    def __init__(self, page: ft.Page):
        super().__init__()
        self.page_singleton = PageSingleton(page)
        self.page = self.page_singleton.page

    def build_sheet_generator_view(self):
        def pick_files_result(e: ft.FilePickerResultEvent):
            if e.files:
                selected_files.value = "Arquivo: " + ", ".join(
                    map(lambda f: f.name, e.files)
                )
                process_button.disabled = False
                process_button.icon_color = ft.colors.INDIGO_ACCENT_400
                process_button.data = e.files[0]
            else:
                selected_files.value = label("no_file_selected")
                process_button.disabled = True
                process_button.icon_color = ft.colors.RED_400
                process_button.data = None

            selected_files.update()
            process_button.update()

        def close_dialog(*args):
            dialog.open = False
            self.page.update()

        def convert_to_musicxml(e):
            file_path = process_button.data.path
            file_name = process_button.data.name

            dialog.modal = True
            dialog.open = True
            dialog.update()

            output_path = MidiManager.generate_scores(file_name, file_path)

            dialog.title = ft.Text(label("convert_end"))
            dialog.content = ft.Container(
                ft.Column(
                    controls=(
                        ft.Text(f"{label('converted_files')}:"),
                        ft.Text(file_name),
                        ft.Divider(),
                        ft.Text(f"{label('saved_successfully')}:"),
                        ft.Text(output_path),
                    ),
                    tight=True,
                )
            )

            dialog.actions = [
                ft.TextButton(label("close"), on_click=close_dialog),
            ]
            dialog.update()

        dialog = ft.AlertDialog(
            title=ft.Text(label("processing")),
            content=ft.Text(label("converting")),
            actions=[],
            actions_alignment=ft.MainAxisAlignment.END,
            open=False,
        )

        self.page.dialog = dialog

        selected_files = ft.Text(
            label("no_file_selected"),
            size=17,
            max_lines=1,
            scale=SETTINGS["accessibility"]["font_scale"],
        )
        process_button = ft.FilledTonalButton(
            label("generate_sheet"),
            icon=ft.icons.QUEUE_MUSIC_OUTLINED,
            disabled=True,
            icon_color=ft.colors.RED_400,
            on_click=convert_to_musicxml,
        )

        pick_files_dialog = ft.FilePicker(on_result=pick_files_result)
        self.page.overlay.append(pick_files_dialog)

        return ft.Row(
            [
                ft.Column(
                    [
                        ft.ElevatedButton(
                            label("select_file"),
                            icon=ft.icons.UPLOAD_FILE_SHARP,
                            on_click=lambda _: pick_files_dialog.pick_files(
                                allow_multiple=False, allowed_extensions=["mid", "midi"]
                            ),
                            tooltip=label("allowed_files"),
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        selected_files,
                        ft.Divider(),
                        ft.Divider(),
                        process_button,
                    ],
                    alignment=ft.MainAxisAlignment.CENTER,
                    horizontal_alignment=ft.CrossAxisAlignment.CENTER,
                )
            ],
            expand=True,
            alignment=ft.MainAxisAlignment.CENTER,
            vertical_alignment=ft.CrossAxisAlignment.CENTER,
        )

    def build_settings_view(self):
        def update_audio_path(e: ft.FilePickerResultEvent):
            if e.path:
                audio_destination_path.value = e.path
                update_destination_path(module="audio", value=e.path)
                audio_destination_path.update()

        audio_destination_path = ft.Text(
            SETTINGS["audio"]["destination_path"],
            size=15,
            max_lines=1,
            scale=SETTINGS["accessibility"]["font_scale"],
        )
        audio_path_dialog = ft.FilePicker(
            on_result=update_audio_path,
        )
        self.page.overlay.append(audio_path_dialog)

        def update_sheets_path(e: ft.FilePickerResultEvent):
            if e.path:
                sheets_destination_path.value = e.path
                update_destination_path(module="sheets", value=e.path)
                sheets_destination_path.update()

        sheets_destination_path = ft.Text(
            SETTINGS["sheets"]["destination_path"],
            size=15,
            max_lines=1,
            scale=SETTINGS["accessibility"]["font_scale"],
        )
        sheets_path_dialog = ft.FilePicker(on_result=update_sheets_path)
        self.page.overlay.append(sheets_path_dialog)

        scale_update_required = ft.Text(
            size=15,
            max_lines=1,
            scale=SETTINGS["accessibility"]["font_scale"],
        )

        def update_app_scale(e):
            update_app_font_scale(e)
            scale_update_required.value = label("restart_required")
            scale_update_required.color = ft.colors.RED_400
            scale_update_required.update()

        language_update_required = ft.Text(
            size=15,
            max_lines=1,
            scale=SETTINGS["accessibility"]["font_scale"],
        )

        def update_app_language(e):
            update_language(e)
            language_update_required.value = label("restart_required")
            language_update_required.color = ft.colors.RED_400
            language_update_required.update()

        return ft.Row(
            [
                ft.Column(
                    [
                        ft.Text(
                            label("general_settings"),
                            size=22,
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        ft.Text(
                            label("language"),
                            size=15,
                            color=ft.colors.GREY_400,
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        ft.Dropdown(
                            options=[
                                ft.dropdown.Option(
                                    key="pt", text="Português (Portuguese)"
                                ),
                                ft.dropdown.Option(key="en", text="English"),
                                ft.dropdown.Option(key="es", text="Español (Spanish)"),
                                ft.dropdown.Option(
                                    key="zh", text="汉语 (Simplified Chinese)"
                                ),
                                ft.dropdown.Option(key="ru", text="Русский (Russian)"),
                                ft.dropdown.Option(key="ja", text="日本語 (Japanese)"),
                            ],
                            value=SETTINGS["general"]["language"],
                            on_change=update_app_language,
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        language_update_required,
                        ft.Container(height=1, width=350, bgcolor=ft.colors.GREY_900),
                        ft.Text(
                            label("audio"),
                            size=22,
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        ft.Text(
                            label("destination_dir"),
                            size=15,
                            color=ft.colors.GREY_400,
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        audio_destination_path,
                        ft.ElevatedButton(
                            label("search"),
                            icon=ft.icons.FOLDER,
                            on_click=lambda _: audio_path_dialog.get_directory_path(),
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        ft.Divider(),
                        ft.Text(
                            label("file_bit_rate"),
                            size=15,
                            color=ft.colors.GREY_400,
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        ft.Dropdown(
                            options=[
                                ft.dropdown.Option(key=44, text="44.1 kHz"),
                                ft.dropdown.Option(key=48, text="48 kHz"),
                                ft.dropdown.Option(key=96, text="96 kHz"),
                                ft.dropdown.Option(key=192, text="192 kHz"),
                            ],
                            value=SETTINGS["audio"]["bit_rate"],
                            on_change=update_bit_rate,
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        ft.Divider(),
                        ft.Text(
                            label("file_bit_depth"),
                            size=15,
                            color=ft.colors.GREY_400,
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        ft.Dropdown(
                            options=[
                                ft.dropdown.Option(key=24, text="24 bits"),
                                ft.dropdown.Option(key=32, text="32 bits"),
                            ],
                            value=SETTINGS["audio"]["bit_depth"],
                            on_change=update_bit_depth,
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        ft.Divider(),
                        ft.Text(
                            label("audio_input"),
                            size=15,
                            color=ft.colors.GREY_400,
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        ft.Dropdown(
                            options=[
                                ft.dropdown.Option(key=0, text="Default"),
                            ],
                            value=SETTINGS["audio"]["input"],
                            on_change=update_input,
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                    ],
                    alignment=ft.MainAxisAlignment.START,
                    horizontal_alignment=ft.CrossAxisAlignment.START,
                ),
                ft.Container(height=700, width=1, bgcolor=ft.colors.GREY_900),
                ft.Column(
                    [
                        ft.Text(
                            label("sheets"),
                            size=22,
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        ft.Text(
                            label("destination_dir"),
                            size=15,
                            color=ft.colors.GREY_400,
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        sheets_destination_path,
                        ft.ElevatedButton(
                            label("search"),
                            icon=ft.icons.FOLDER,
                            on_click=lambda _: sheets_path_dialog.get_directory_path(),
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        ft.Divider(),
                        ft.Text(
                            label("sheet_style"),
                            size=15,
                            color=ft.colors.GREY_400,
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        ft.Dropdown(
                            options=[
                                ft.dropdown.Option(key="staff", text=label("staff")),
                                ft.dropdown.Option(
                                    key="tablature", text=label("tablature")
                                ),
                            ],
                            value=SETTINGS["sheets"]["style"],
                            on_change=update_sheets_style,
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        ft.Divider(),
                        ft.Text(
                            label("font_size"),
                            size=15,
                            color=ft.colors.GREY_400,
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        ft.Dropdown(
                            options=[
                                ft.dropdown.Option(key=i, text=str(i))
                                for i in range(12, 37)
                            ],
                            value=SETTINGS["sheets"]["font_size"],
                            on_change=update_sheets_font_size,
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        ft.Divider(),
                        ft.Container(height=1, width=350, bgcolor=ft.colors.GREY_900),
                        ft.Text(
                            label("accessibility"),
                            size=22,
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        ft.Text(
                            label("font_scale"),
                            size=15,
                            color=ft.colors.GREY_400,
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        ft.Dropdown(
                            options=[
                                ft.dropdown.Option(key=1.0, text=label("font_normal")),
                                ft.dropdown.Option(key=1.2, text=label("font_large")),
                            ],
                            value=SETTINGS["accessibility"]["font_scale"],
                            on_change=update_app_scale,
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        scale_update_required,
                        ft.Container(height=1, width=350, bgcolor=ft.colors.GREY_900),
                        ft.Text(
                            label("help"),
                            size=22,
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        ft.Text(
                            "FAQ",
                            size=15,
                            color=ft.colors.GREY_400,
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                        ft.Text(
                            label("user_manual"),
                            size=15,
                            color=ft.colors.GREY_400,
                            scale=SETTINGS["accessibility"]["font_scale"],
                        ),
                    ],
                    alignment=ft.MainAxisAlignment.START,
                    horizontal_alignment=ft.CrossAxisAlignment.START,
                ),
            ],
            expand=True,
            alignment=ft.MainAxisAlignment.SPACE_EVENLY,
            vertical_alignment=ft.CrossAxisAlignment.CENTER,
            spacing=150,
        )

    def _handle_image_click(self, file_name: str):
        os.startfile(
            os.path.join(SETTINGS["sheets"]["destination_path"], f"{file_name}")
        )

    def build_sheets_view(self, files: list[str] = None):
        if not files:
            return ft.Row(
                [
                    ft.Column(
                        [
                            ft.Text(
                                label("no_sheet_found"),
                                size=25,
                                scale=SETTINGS["accessibility"]["font_scale"],
                            )
                        ],
                        alignment=ft.MainAxisAlignment.CENTER,
                        horizontal_alignment=ft.CrossAxisAlignment.CENTER,
                    )
                ],
                expand=True,
                alignment=ft.MainAxisAlignment.CENTER,
                vertical_alignment=ft.CrossAxisAlignment.CENTER,
            )

        available_files = []

        for file in files:
            available_files.append(
                ft.TextButton(
                    on_click=lambda _, f=file: self._handle_image_click(f),
                    content=ft.Column(
                        [
                            ft.Image(
                                src="/icons/sheetfile.png",
                                width=50,
                                height=50,
                                fit=ft.ImageFit.COVER,
                            ),
                            ft.Text(
                                SettingsFileManager.shorten_filename(file),
                                size=14,
                                scale=SETTINGS["accessibility"]["font_scale"],
                            ),
                        ],
                        alignment=ft.MainAxisAlignment.SPACE_EVENLY,
                        horizontal_alignment=ft.CrossAxisAlignment.CENTER,
                    ),
                )
            )

        return ft.Row(
            [*available_files],
            wrap=True,
            spacing=10,
            run_spacing=30,
            width=self.page.window_width,
            height=self.page.window_height,
            scroll=ft.ScrollMode.ALWAYS,
            expand=6,
            alignment=ft.MainAxisAlignment.SPACE_EVENLY,
            vertical_alignment=ft.CrossAxisAlignment.CENTER,
        )

    def build_audio_view(self):
        audio_row = ft.Row(
            [],
            wrap=True,
            spacing=10,
            run_spacing=30,
            width=self.page.window_width,
            height=self.page.window_height,
            scroll=ft.ScrollMode.ALWAYS,
            expand=6,
            alignment=ft.MainAxisAlignment.SPACE_EVENLY,
            vertical_alignment=ft.CrossAxisAlignment.CENTER,
        )

        audio_controller = AudioController(self.page)
        select_audios = SelectAudios(self.page, audio_row)
        rail = Rail(self.page)

        bottom_bar = BottomBar(
            self.page,
            select_audios.button,
            audio_controller.controller,
            audio_controller.volume_controller,
        )

        self.page.bottom_appbar = bottom_bar.component

        return ft.Row(
            [
                audio_row,
                rail.component,
            ],
            expand=True,
        )
