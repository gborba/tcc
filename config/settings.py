from .utils import SettingsFileManager

file_manager = SettingsFileManager()
file_manager._read_settings()

SETTINGS = file_manager.settings


@staticmethod
def label(key: str) -> dict:
    return (
        file_manager.app_labels[SETTINGS["general"]["language"]].get(key)
        or f"MISSING: {key}"
    )


@staticmethod
def normalize_string(s):
    if len(s) > 10:
        shortened_string = s[:10]
        return shortened_string.rstrip() + "..." + s[-3:]
    else:
        return s


@staticmethod
def get_files(path: str, extensions: list[str]) -> list[str]:
    return file_manager.get_files_with_extensions(
        directory_path=path, extensions=extensions
    )


@staticmethod
def update_destination_path(module: str, value: str):
    file_manager.update_settings_file(
        module=module, key="destination_path", value=value
    )


@staticmethod
def update_language(e) -> None:
    file_manager.update_settings_file(
        module="general", key="language", value=e.control.value
    )


@staticmethod
def update_bit_rate(e) -> None:
    file_manager.update_settings_file(
        module="audio", key="bit_rate", value=int(e.control.value)
    )


@staticmethod
def update_bit_depth(e) -> None:
    file_manager.update_settings_file(
        module="audio", key="bit_depth", value=int(e.control.value)
    )


@staticmethod
def update_input(e) -> None:
    file_manager.update_settings_file(
        module="audio", key="input", value=int(e.control.value)
    )


@staticmethod
def update_sheets_style(e) -> None:
    file_manager.update_settings_file(
        module="sheets", key="style", value=e.control.value
    )


@staticmethod
def update_sheets_font_size(e) -> None:
    file_manager.update_settings_file(
        module="sheets", key="font_size", value=int(e.control.value)
    )


@staticmethod
def update_app_font_scale(e) -> None:
    file_manager.update_settings_file(
        module="accessibility", key="font_scale", value=float(e.control.value)
    )
