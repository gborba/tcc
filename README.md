Instalação
Pré-requisitos

- Python 3.11.
- pip (instalador de pacotes Python) instalado e disponível nas variáveis de ambiente de ambiente do sistema.
- ffmpeg instalado e disponível nas variáveis de ambiente de ambiente do sistema (https://www.ffmpeg.org/download.html).
- Musescore versão 4 instalado e disponível nas variáveis de ambiente do sistema (https://musescore.org/download).

Passos para Instalação e Execução

1. Navegue até o diretório do projeto

2. Crie um ambiente virtual
	`python -m venv venv`

3. Ative o ambiente virtual
	No Windows:

	`venv\Scripts\activate`
	
	No macOS/Linux:

	`source venv/bin/activate`
	
4. Instale as dependências necessárias
	`pip install -r requirements.txt`
	
5. Execute a aplicação
	`flet run`
	
Compilação:

1. A partir do ambiente virtual já criado e com as dependências instaladas, execute:
	`flet build {macos,linux,windows,web,apk,aab,ipa}`
	
	Exemplo:
	
	`flet build linux`