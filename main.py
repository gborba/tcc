import flet as ft

from view import ViewRouter
from config.settings import label


class SongtrackApp(ft.UserControl):
    def __init__(self, page: ft.Page):
        super().__init__()
        self.page = page

        self.page.title = "Songtrack"
        self.page.window_min_height = 900
        self.page.window_min_width = 1100

        self.page.horizontal_alignment = self.page.vertical_alignment = "center"
        self.page.vertical_alignment = self.page.vertical_alignment = "center"

        self.page.fonts = {
            "Josefin": "https://github.com/googlefonts/josefinsans/raw/master/fonts/ttf/JosefinSans-SemiBold.ttf",
            "Major Mono Display": "https://github.com/googlefonts/majormono/raw/master/fonts/MajorMonoDisplay-Regular.ttf",
            "Zen Tokyo": "https://github.com/googlefonts/zen-tokyo-zoo/raw/main/fonts/ttf/ZenTokyoZoo-Regular.ttf",
        }

        self.page.theme = ft.Theme(
            font_family="Josefin", color_scheme_seed=ft.colors.INDIGO_ACCENT_400
        )

        self.page.drawer = ft.NavigationDrawer(
            on_change=self._change_view,
            elevation=40,
            controls=[
                ft.Container(height=12),
                ft.NavigationDrawerDestination(
                    label=label("mixer"),
                    icon=ft.icons.AUDIO_FILE_OUTLINED,
                    selected_icon_content=ft.Icon(ft.icons.AUDIO_FILE),
                ),
                ft.Divider(thickness=2),
                ft.NavigationDrawerDestination(
                    label=label("generate_sheet_drawer"),
                    icon=ft.icons.MUSIC_NOTE_OUTLINED,
                    selected_icon_content=ft.Icon(ft.icons.MUSIC_NOTE),
                ),
                ft.NavigationDrawerDestination(
                    label=label("saved_sheets_drawer"),
                    icon=ft.icons.BOOK_OUTLINED,
                    selected_icon_content=ft.Icon(ft.icons.BOOK),
                ),
                ft.NavigationDrawerDestination(
                    label=label("settings"),
                    icon=ft.icons.SETTINGS_OUTLINED,
                    selected_icon_content=ft.Icon(ft.icons.SETTINGS),
                ),
            ],
        )

        self.page.appbar = ft.AppBar(
            leading=ft.IconButton(
                icon=ft.icons.VIEW_HEADLINE, on_click=self._show_drawer
            ),
            title=ft.Text(
                font_family="Zen Tokyo",
                size=40,
                color=ft.colors.INDIGO_ACCENT_400,
                spans=[ft.TextSpan("SONGTRACK")],
            ),
            center_title=True,
            bgcolor=ft.colors.BLACK,
            toolbar_height=70,
        )

    def _show_drawer(self, e):
        self.page.drawer.open = True
        self.page.drawer.update()

    def _change_view(self, e):
        index = e.control.selected_index

        match index:
            case 0:
                self.layout.set_audio_view()
            case 1:
                self.layout.set_sheet_generator_view()
            case 2:
                self.layout.set_sheets_view()
            case 3:
                self.layout.set_settings_view()

        self.page.update()

    def build(self):
        self.layout = ViewRouter(
            self,
            self.page,
        )
        return self.layout


def main(page: ft.Page):
    app = SongtrackApp(page)
    page.add(app)


ft.app(main, assets_dir="assets")
