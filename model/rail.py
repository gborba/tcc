import flet as ft

from config.settings import label


class Rail(ft.UserControl):
    def __init__(
        self,
        page: ft.Page,
    ) -> None:
        super().__init__()
        self.page = page

        self.split_selected_button = self.get_rail_icon(
            icon=ft.icons.MULTITRACK_AUDIO_OUTLINED,
            tooltip=label("split_selected"),
            action=self.split_selected,
        )
        self.merge_button = self.get_rail_icon(
            icon=ft.icons.JOIN_INNER_OUTLINED,
            tooltip=label("merge_selected"),
            action=self.merge_selected,
        )
        self.toggle_all_button = self.get_rail_icon(
            icon=ft.icons.CHECKLIST_OUTLINED,
            tooltip=label("toggle_all"),
            action=self.toggle_all,
        )
        self.remove_selected_button = self.get_rail_icon(
            icon=ft.icons.PLAYLIST_REMOVE_OUTLINED,
            tooltip=label("remove_selected"),
            action=self.remove_selected,
        )
        self.sync_selected_button = self.get_rail_icon(
            icon=ft.icons.SYNC_OUTLINED,
            tooltip=label("sync_selected"),
            action=self.sync_selected,
        )
        self.restart_selected_button = self.get_rail_icon(
            icon=ft.icons.RESTART_ALT_SHARP,
            tooltip=label("restart_selected"),
            action=self.restart_selected,
        )

        self.component = ft.Column(
            [
                # ft.Container(height=10),
                self.split_selected_button,
                self.merge_button,
                self.sync_selected_button,
                self.toggle_all_button,
                self.restart_selected_button,
                self.remove_selected_button,
            ],
            alignment=ft.MainAxisAlignment.START,
            horizontal_alignment=ft.CrossAxisAlignment.CENTER,
            height=self.page.window_height,
            visible=False,
        )

        self._sub_message("show_rail", self.show_rail)
        self._sub_message("hide_rail", self.hide_rail)

    @classmethod
    def get_rail_icon(cls, icon: ft.icons, tooltip: str, action: callable):
        return ft.IconButton(
            content=ft.Container(
                content=ft.Column(
                    [
                        ft.IconButton(
                            icon=icon,
                            icon_color=ft.colors.WHITE,
                            tooltip=tooltip,
                            on_click=action,
                        ),
                        ft.Text(value=tooltip),
                    ],
                    alignment=ft.MainAxisAlignment.CENTER,
                    horizontal_alignment=ft.CrossAxisAlignment.CENTER,
                    spacing=5,
                ),
                padding=ft.padding.all(10),
            ),
            on_click=action,
        )

    def split_selected(self, _):
        self._pub_message("split_selected", True)

    def merge_selected(self, _):
        self._pub_message("merge_selected", True)

    def toggle_all(self, _):
        icon: ft.IconButton = self.toggle_all_button.content.content.controls[0]

        if icon.icon_color != ft.colors.GREEN_ACCENT_400:
            icon.icon_color = ft.colors.GREEN_ACCENT_400
            self._pub_message("toggle_all", True)
        else:
            icon.icon_color = ft.colors.WHITE
            self._pub_message("toggle_all", False)

        self.toggle_all_button.update()

    def remove_selected(self, _):
        self._pub_message("remove_selected", True)

    def sync_selected(self, _):
        icon: ft.IconButton = self.sync_selected_button.content.content.controls[0]

        if icon.icon_color != ft.colors.GREEN_ACCENT_400:
            icon.icon_color = ft.colors.GREEN_ACCENT_400
            self._pub_message("sync_selected", True)
        else:
            icon.icon_color = ft.colors.WHITE
            self._pub_message("sync_selected", False)

        self.sync_selected_button.update()

    def restart_selected(self, _):
        self._pub_message("restart_selected", True)

    def _sub_message(self, topic: str, handler: callable):
        self.page.pubsub.subscribe_topic(topic, handler)

    def _pub_message(self, topic: str, data):
        self.page.pubsub.send_all_on_topic(topic, data)
        self.page.update()

    def _pub_message(self, topic: str, data):
        self.page.pubsub.send_all_on_topic(topic, data)
        self.page.update()

    def show_rail(self, *args):
        self.component.visible = True
        self.component.update()

    def hide_rail(self, *args):
        self.component.visible = False
        self.component.update()
