import flet as ft


class BottomBar(ft.Row):
    def __init__(
        self,
        page: ft.Page,
        select_audios_button: ft.ElevatedButton,
        audio_controller: ft.IconButton,
        volume_controller: ft.Row,
    ) -> None:
        super().__init__()
        self.page = page
        self.component = ft.BottomAppBar(
            bgcolor=ft.colors.BLACK,
            height=100,
            shape=ft.NotchShape.CIRCULAR,
            content=ft.Row(
                [select_audios_button, audio_controller, volume_controller],
                expand=True,
                alignment=ft.MainAxisAlignment.SPACE_BETWEEN,
                vertical_alignment=ft.CrossAxisAlignment.CENTER,
            ),
        )
