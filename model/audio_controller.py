from time import sleep
from collections import defaultdict

import flet as ft

from config.settings import label
from services import MergeAudio, SplitAudioSources


class AudioController(ft.UserControl):
    def __init__(self, page: ft.Page) -> None:
        super().__init__()

        self.page = page

        self.selected_audios: list[str] = []
        self.currently_playing: list[str] = []
        self.currently_processing: dict = self.process_dto()

        self.dialog = None
        self.audio_volume: int = 1
        self.seek_sync: bool = False

        self.play_icon = ft.icons.PLAY_CIRCLE_FILL_SHARP
        self.pause_icon = ft.icons.PAUSE_CIRCLE_FILLED_SHARP
        self.volume_icon = ft.icons.VOLUME_UP_SHARP
        self.mute_icon = ft.icons.VOLUME_OFF_SHARP

        self.controller = ft.IconButton(
            icon=ft.icons.PLAY_CIRCLE_FILL_SHARP,
            icon_color=ft.colors.WHITE,
            scale=2,
            on_click=self.audio_controller,
            disabled=True,
            tooltip=label("select_files"),
        )

        self.mute_button = ft.IconButton(
            icon=ft.icons.VOLUME_UP_SHARP,
            icon_color=ft.colors.WHITE,
            tooltip=label("mute"),
            on_click=self.mute_control,
        )

        self.volume_slider = ft.Slider(
            min=0,
            max=1,
            value=self.audio_volume,
            inactive_color=ft.colors.WHITE,
            active_color=ft.colors.INDIGO_ACCENT_400,
            on_change=self.change_volume,
        )

        self.volume_controller = ft.Row(
            [
                self.mute_button,
                self.volume_slider,
            ],
            spacing=0,
            scale=0.8,
            alignment=ft.MainAxisAlignment.END,
            vertical_alignment=ft.CrossAxisAlignment.CENTER,
        )

        self.split_audio_service = SplitAudioSources(self.page)

        ## FILE_MANAGER

        self._sub_message("song_ended", self.song_ended)
        self._sub_message("select_audio", self.select_audio)
        self._sub_message("unselect_audio", self.unselect_audio)
        self._sub_message("seek_audio", self.seek_audio)
        self._sub_message("card_removed", self.card_removed)

        ## RAIL

        self._sub_message("split_selected", self.split_selected)
        self._sub_message("merge_selected", self.merge_selected)
        self._sub_message("remove_selected", self.remove_selected)
        self._sub_message("sync_selected", self.sync_selected)
        self._sub_message("restart_selected", self.restart_selected)

        ## AUDIO_PROCESSING

        self._sub_message("file_status", self.processing_status)
        self._sub_message("cancel_end", self.processing_cancel)

    @classmethod
    def get_alert_dialog(cls, title: str = None, content: str = None) -> ft.AlertDialog:
        return ft.AlertDialog(
            title=ft.Text(title),
            content=ft.Text(content),
            actions_alignment=ft.MainAxisAlignment.END,
            open=True,
        )

    @classmethod
    def process_dto(cls):
        return defaultdict(lambda: {"iteration": 1, "progress": 0, "status": ""})

    def get_file_checkbox(
        self, file_path: str = None, content: str = None
    ) -> ft.Checkbox:
        path_split = file_path.split("\\")
        file_name = path_split[-1]

        if len(file_name) > 40:
            file_name = f"{file_name[:40].rstrip()}..."

        return ft.Checkbox(
            label=file_name,
            value=True,
            active_color=ft.colors.GREEN_ACCENT_400,
            check_color=ft.colors.GREY_900,
            on_change=self.process_audio,
            data=file_path,
        )

    def processing_cancel(self, *args):
        self.page.close_dialog()

        self.currently_processing = self.process_dto()
        self.split_audio_service.force_stop = False

    def processing_status(self, _, data: dict):
        def close_dialog(*args):
            dialog.open = False
            self.page.update()

        def cancel_job(*args):
            self.split_audio_service.force_stop = True
            dialog.content = ft.Text(label("cancelling"))
            dialog.actions = None
            self.page.update()

        dialog = self.get_alert_dialog(title=label("processing"))
        dialog.modal = True

        file_name = data["file_name"]
        progress = data["progress"]
        status = data["status"]

        if progress < self.currently_processing[file_name]["progress"]:
            self.currently_processing[file_name]["iteration"] += 1

        self.currently_processing[file_name]["progress"] = progress
        self.currently_processing[file_name]["status"] = status

        if status == "ALL_FINISHED":
            dialog.title = ft.Text(label("job_finished"))
            dialog.actions = [
                ft.TextButton(label("close"), on_click=close_dialog),
            ]
            self.currently_processing = self.process_dto()
        else:
            queued_data = []
            in_progress_data = []
            finished_data = []

            for file_name, processing_data in self.currently_processing.items():
                if processing_data["status"] == "QUEUED":
                    queued_data.append(ft.Text(file_name))
                elif processing_data["status"] == "IN_PROGRESS":
                    in_progress_data.append(ft.Text(file_name))
                    in_progress_data.append(
                        ft.ProgressBar(value=processing_data["progress"])
                    )
                    in_progress_data.append(
                        ft.Text(
                            f"{label('iteration')}: ({processing_data['iteration']}/4) - {int(processing_data['progress'] * 100)}% "
                        )
                    )

                else:
                    finished_data.append(ft.Text(file_name))

            queued = (
                [
                    ft.Text(f"{label('queued')}"),
                    ft.Container(ft.Column(controls=[*queued_data])),
                ]
                if queued_data
                else []
            )
            in_progress = (
                [
                    ft.Divider(),
                    ft.Text(f"{label('in_progress')}"),
                    ft.Container(ft.Column(controls=[*in_progress_data])),
                ]
                if in_progress_data
                else []
            )
            finished = (
                [
                    ft.Divider(),
                    ft.Text(f"{label('finished')}"),
                    ft.Container(ft.Column(controls=[*finished_data])),
                ]
                if finished_data
                else []
            )

            dialog.actions = [ft.TextButton(label("cancel"), on_click=cancel_job)]

            dialog.content = ft.Container(
                ft.Column(
                    controls=[
                        *queued,
                        *in_progress,
                        *finished,
                    ],
                    tight=True,
                )
            )

        self.page.dialog = dialog
        self.page.update()

    def process_audio(self, e):
        self._pub_message("update_card", e)

    def split_selected(self, *args):
        def close_dialog(*args):
            dialog.open = False
            self.page.update()

        def proceed_split(*args):
            dialog.open = False
            self.page.update()

            for audio in self.selected_audios:
                self.currently_processing[audio]["status"] = "QUEUED"

            self.split_audio_service.separate(self.selected_audios)

        dialog = self.get_alert_dialog()
        self.page.dialog = dialog

        if self.selected_audios:
            files = []
            for audio in self.selected_audios:
                files.append(self.get_file_checkbox(audio))

            dialog.title = ft.Text(label("split_confirm"))
            dialog.content = ft.Container(
                ft.Column(
                    controls=(ft.Text(f"{label('confirm_split')}:"), *files), tight=True
                )
            )
            dialog.modal = True
            dialog.actions = [
                ft.TextButton(label("no"), on_click=close_dialog),
                ft.TextButton(label("yes"), on_click=proceed_split),
            ]
        else:
            dialog.title = ft.Text(label("unsufficient_files"))
            dialog.content = ft.Text(label("select_1_file"))
            sleep(2)
            dialog.open = False
            self.page.update()

    def merge_selected(self, *args):
        def close_dialog(*args):
            dialog.open = False
            self.page.update()

        def proceed_merge(*args):
            dialog.title = ft.Text(label("processing"))
            dialog.modal = True
            dialog.actions = []
            dialog.content = ft.Text(label("merging"))

            dialog.update()

            output_path = MergeAudio.merge(self.selected_audios)

            dialog.title = ft.Text(label("merge_end"))
            files = [ft.Text(file) for file in self.selected_audios]

            dialog.content = ft.Container(
                ft.Column(
                    controls=(
                        ft.Text(f"{label('merged_files')}:"),
                        *files,
                        ft.Divider(),
                        ft.Text(f"{label('saved_successfully')}: {output_path}"),
                    ),
                    tight=True,
                )
            )

            dialog.actions = [
                ft.TextButton(label("close"), on_click=close_dialog),
            ]

            dialog.update()

        dialog = self.get_alert_dialog()
        self.page.dialog = dialog

        if len(self.selected_audios) < 2:
            dialog.title = ft.Text(label("unsufficient_files"))
            dialog.content = ft.Text(label("select_2_files"))
            self.page.update()
            sleep(2)
            dialog.open = False
            self.page.update()
        else:
            dialog.title = ft.Text(label("merge_confirm"))
            dialog.content = ft.Text(label("confirm_merge"))
            dialog.modal = True
            dialog.actions = [
                ft.TextButton(label("no"), on_click=close_dialog),
                ft.TextButton(label("yes"), on_click=proceed_merge),
            ]

    def sync_selected(self, _, sync: bool):
        self.seek_sync = sync

    def remove_selected(self, *args):
        def close_dialog(*args):
            dialog.open = False
            self.page.update()

        def proceed_delete(*args):
            self._pub_message("remove_cards", self.selected_audios)

            for audio in self.selected_audios:
                if control := self._get_audio_control(audio):
                    control.pause()
                    self.page.overlay.remove(control)

            self.selected_audios = []
            self.currently_playing = []

            self.controller.icon = self.play_icon
            self._disable_controllers()

            self._refresh()
            close_dialog()

        def unselected_audio():
            dialog.title = ft.Text(label("no_file_selected"))
            dialog.content = ft.Text(label("select_1_file"))
            self.page.dialog = dialog
            self.page.update()
            sleep(2)
            close_dialog()

        dialog = self.get_alert_dialog(
            title=label("confirm"), content=label("confirm_removal")
        )

        if self.selected_audios:
            dialog.modal = True
            dialog.actions = [
                ft.TextButton(label("no"), on_click=close_dialog),
                ft.TextButton(label("yes"), on_click=proceed_delete),
            ]
            self.page.dialog = dialog
            self.page.update()
        else:
            unselected_audio()

    def restart_selected(self, *args):
        for audio in self.selected_audios:
            control = self._get_audio_control(audio)
            control.seek(0)

    def mute_control(self, e):
        volume = self._get_volume(e.control.icon)

        for item in self.page.overlay:
            if isinstance(item, ft.Audio):
                item.volume = volume
                item.update()

        self.volume_slider.value = volume
        self.volume_slider.update()

    def _get_volume(self, icon, volume: int = None) -> int:
        if icon == self.volume_icon:
            self.mute_button.icon = self.mute_icon
            volume = 0
        else:
            self.mute_button.icon = self.volume_icon
            volume = self.audio_volume

        self.mute_button.update()
        return volume

    def _update_volume_icon(self):
        self.mute_button.icon = (
            self.volume_icon if self.audio_volume else self.mute_icon
        )
        self.mute_button.update()

    def change_volume(self, e):
        self.audio_volume = e.control.value

        for item in self.page.overlay:
            if isinstance(item, ft.Audio):
                item.volume = self.audio_volume
                item.update()

        self._update_volume_icon()

    def song_ended(self, _, audio: str):
        self.currently_playing.remove(audio)

        if not self.currently_playing:
            self.controller.icon = self.play_icon

        if not self.selected_audios:
            self._disable_controllers()

        self._refresh()

    def select_audio(self, _, audio: str):
        self._enable_controllers()
        self.selected_audios.append(audio)

        control = self._get_audio_control(audio)
        control.volume = self.audio_volume

        if self.controller.icon == self.pause_icon:
            self.currently_playing.append(audio)
            control.resume()

        self._refresh()

    def unselect_audio(self, _, audio: str):
        self.selected_audios.remove(audio)

        if audio in self.currently_playing:
            self.currently_playing.remove(audio)
            control = self._get_audio_control(audio)
            control.pause()

        if not self.selected_audios:
            self.controller.icon = self.play_icon
            self._disable_controllers()

        self._refresh()

    def seek_audio(self, _, data: tuple[str, int]):
        if self.seek_sync:
            for audio in self.selected_audios:
                control = self._get_audio_control(audio)
                control.seek(int(data[1]))
        else:
            control = self._get_audio_control(data[0])
            control.seek(int(data[1]))

    def card_removed(self, _, audio: str):
        control = self._get_audio_control(audio)
        control.pause()
        self.page.overlay.remove(control)

        if audio in self.currently_playing:
            self.currently_playing.remove(audio)

        if audio in self.selected_audios:
            self.selected_audios.remove(audio)

        if not self.selected_audios:
            self.controller.icon = self.play_icon
            self._disable_controllers()

        self._refresh()

    def audio_controller(self, e):
        if self.currently_playing:
            for control in self.page.overlay:
                if control.data in self.currently_playing:
                    control.pause()

            self.currently_playing = []
            self.controller.icon = self.play_icon
        else:
            for control in self.page.overlay:
                if (
                    isinstance(control, ft.Audio)
                    and control.data in self.selected_audios
                ):
                    control.resume()
                    self.currently_playing.append(control.data)
                    self.controller.icon = self.pause_icon

        (
            self._enable_controllers()
            if self.selected_audios
            else self._disable_controllers
        )
        self._refresh()

    def _enable_controllers(self):
        self.controller.disabled = False
        self.tooltip = None

    def _disable_controllers(self):
        self.controller.disabled = True
        self.controller.tooltip = label("select_files")

    def _refresh(self):
        self.controller.update()
        self.page.update()

    def _pub_message(self, topic: str, data):
        self.page.pubsub.send_all_on_topic(topic, data)
        self.page.update()

    def _sub_message(self, topic: str, handler: callable):
        self.page.pubsub.subscribe_topic(topic, handler)

    def _get_audio_control(self, audio: str):
        for control in self.page.overlay:
            if isinstance(control, ft.Audio) and control.data == audio:
                return control
