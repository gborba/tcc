from time import sleep

import flet as ft

from config.settings import label, SETTINGS
from model.custom_default import CustomCard


class SelectAudios(ft.UserControl):
    def __init__(self, page: ft.Page, audio_row: ft.Row) -> None:
        super().__init__()

        self.page = page
        self.audio_row = audio_row

        self.cards: dict[str, ft.Card] = {}
        self.card_count = 0

        self.pick_files_dialog = ft.FilePicker(on_result=self.pick_files_result)
        self.page.overlay.append(self.pick_files_dialog)

        self.button = ft.ElevatedButton(
            label("select_files"),
            icon=ft.icons.UPLOAD_FILE,
            on_click=lambda _: self.pick_files_dialog.pick_files(
                allow_multiple=True, allowed_extensions=["mp3", "wav"]
            ),
            tooltip=label("allowed_files"),
            scale=SETTINGS["accessibility"]["font_scale"],
            icon_color=ft.colors.WHITE,
            bgcolor=ft.colors.BLACK,
            color=ft.colors.WHITE,
        )

        self._sub_message("remove_cards", self.remove_cards)
        self._sub_message("toggle_all", self.toggle_all)
        self._sub_message("update_card", self.update_card)

    def update_audio_position(self, e):
        if card := self.cards.get(e.control.data):
            slider: ft.Slider = card.content.content.controls[1].controls[1]
            slider.value = e.data

            total_time = int(slider.max) / 1000
            current_time = int(e.data) / 1000

            total_minutes, total_seconds = divmod(total_time, 60)
            current_minutes, current_seconds = divmod(current_time, 60)

            slider.tooltip = f"{int(current_minutes)}:{int(current_seconds)} - {int(total_minutes)}:{int(total_seconds)}"

            slider.update()

    def toggle_all(self, _, toggle: bool):
        for card in self.cards.values():
            checkbox: ft.Checkbox = card.content.content.controls[1].controls[0]

            if toggle and not checkbox.value:
                checkbox.value = True
                self._pub_message("select_audio", checkbox.data)
            elif not toggle and checkbox.value:
                checkbox.value = False
                self._pub_message("unselect_audio", checkbox.data)

            checkbox.update()

    def song_ended(self, e):
        if e.data == "completed":
            self._pub_message("song_ended", e.control.data)
            slider: ft.Slider = (
                self.cards[e.control.data].content.content.controls[1].controls[1]
            )
            slider.value = 0
            slider.update()

    def update_card(self, _, e):
        checkbox: ft.Checkbox = (
            self.cards[e.control.data].content.content.controls[1].controls[0]
        )

        if e.control.value:
            checkbox.value = True
            self._pub_message("select_audio", e.control.data)
        else:
            checkbox.value = False
            self._pub_message("unselect_audio", e.control.data)

        checkbox.update()

    def mix_audio(self, e):
        if e.control.value:
            self._pub_message("select_audio", e.control.data)
        else:
            self._pub_message("unselect_audio", e.control.data)

    def seek_audio(self, e):
        self._pub_message("seek_audio", (e.control.data, e.control.value))

    def remove_card(self, e):
        for control in self.controls:
            if control.data == e.control.data:
                if isinstance(control, ft.Audio):
                    control.pause()
                self.controls.remove(control)

        for control in self.audio_row.controls:
            if control.data == e.control.data:
                self.audio_row.controls.remove(control)
                self.audio_row.update()

        del self.cards[e.control.data]
        self.card_count -= 1

        if not self.card_count:
            self._pub_message("hide_rail", True)

        self._pub_message("card_removed", e.control.data)

    def remove_cards(self, _, audios):
        for audio in audios:
            for control in self.audio_row.controls:
                if audio == control.data:
                    self.audio_row.controls.remove(control)
                    self.audio_row.update()

            del self.cards[audio]
            self.card_count -= 1

        if not self.card_count:
            self._pub_message("hide_rail", True)

    def _pub_message(self, topic: str, data):
        self.page.pubsub.send_all_on_topic(topic, data)
        self.page.update()

    def _sub_message(self, topic: str, handler: callable):
        self.page.pubsub.subscribe_topic(topic, handler)

    def pick_files_result(self, e: ft.FilePickerResultEvent):
        if files := e.files:
            for file in files:
                audio = ft.Audio(
                    src=file.path,
                    autoplay=False,
                    volume=100,
                    balance=0,
                    data=file.path,
                    on_position_changed=self.update_audio_position,
                    on_state_changed=self.song_ended,
                )

                if audio not in self.page.overlay:
                    self.page.overlay.append(audio)
                    self.page.update()

                    sleep(0.5)
                    duration = audio.get_duration()

                    card = CustomCard(
                        content=ft.Container(
                            content=ft.Column(
                                [
                                    ft.ListTile(
                                        leading=ft.Image(
                                            src="/icons/audiofile.png",
                                            width=40,
                                            height=40,
                                            fit=ft.ImageFit.COVER,
                                            data=file.path,
                                        ),
                                        title=ft.Text(file.name),
                                        subtitle=ft.Text(file.path),
                                    ),
                                    ft.Row(
                                        [
                                            ft.Checkbox(
                                                label=label("mix_file"),
                                                value=False,
                                                active_color=ft.colors.INDIGO_ACCENT_400,
                                                check_color=ft.colors.GREY_900,
                                                tooltip=file.path,
                                                on_change=self.mix_audio,
                                                data=file.path,
                                            ),
                                            ft.Slider(
                                                min=0,
                                                max=duration,
                                                inactive_color=ft.colors.GREY_500,
                                                active_color=ft.colors.WHITE,
                                                on_change=self.seek_audio,
                                                data=file.path,
                                            ),
                                            ft.IconButton(
                                                icon=ft.icons.DELETE_SWEEP_OUTLINED,
                                                icon_color=ft.colors.WHITE,
                                                tooltip=label("remove_file"),
                                                data=file.path,
                                                on_click=self.remove_card,
                                            ),
                                        ],
                                        alignment=ft.MainAxisAlignment.SPACE_EVENLY,
                                    ),
                                ]
                            ),
                            width=500,
                            padding=10,
                            data=file.path,
                        )
                    )

                    if not self.card_count:
                        self._pub_message("show_rail", True)

                    self.cards[file.path] = card
                    self.card_count += 1

                    self.audio_row.controls.append(card)
                    self.audio_row.update()

                    self.page.update()
