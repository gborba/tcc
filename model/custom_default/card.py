import flet as ft


class CustomCard(ft.Card):
    def __init__(self, content):
        super().__init__(content)
        self.data = content.data

    def __eq__(self, other):
        return isinstance(other, ft.Card) and hasattr(other, "data") and self.data == other.data

    def __hash__(self):
        return hash(self.data)
