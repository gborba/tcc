import flet as ft


class CustomCupertinoCheckbox(ft.CupertinoCheckbox):
    def __eq__(self, other):
        return (
            isinstance(other, ft.CupertinoCheckbox)
            and self.label == other.label
            and self.data == other.data
            and self.tooltip == other.tooltip
        )

    def __hash__(self):
        return hash((self.label, self.data))
